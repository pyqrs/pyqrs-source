# -*- mode: python3 ; coding: utf-8 -*-

block_cipher = None

a = Analysis(['pyqrs.py'],
             binaries=[],
			 datas=[
				   ('cursor_blue_transparant.png','.'),
				   ('cursor_red_transparant.png','.'),
				   ('pyqrs.kv','.'),
				   ('PyQRS.ico','.')
				   ],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=['numpy'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='PyQRS',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False,
          runtime_tmpdir=None,
          icon='PyQRS.ico'
       )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas, 
               strip=False,
               upx=True,
               upx_exclude=[],
               name='PyQRS')
